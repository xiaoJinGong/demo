/*
 * @Description: 
 * @Author: Jino
 * @Date: 2020-06-03 16:11:48
 * @LastEditors: Jeddy
 * @LastEditTime: 2020-07-23 11:18:07
 */ 
module.exports = {
  devServer: {
    port: '8080',
    open: true,
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: 'https://m.591.com.tw',
        // target: 'https://union.591.com.tw/', // 测试广告拦截问题
        changeOrigin: true,
        pathRewrite: {'^/api': 'https://m.591.com.tw'}
        // pathRewrite: {'^/api': 'https://union.591.com.tw/'}
      }
    }
  }
}