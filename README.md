# demo

## 项目设置初始
```
npm install
```

### 编译运行开发环境
```
npm run serve
```

### 编译运行生成环境
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### 代码规范

1. 非data的属性以下划线开头（_）

2. 非method方法以下划线开头（_）

3. 页面以及组件文件名采用大驼峰式命名法（upper camel case）。例如：FirstName.vue、LastName.vue

   js文件名采用小驼峰式命名法（lower camel case），例如：lastName.js

4. 文件夹采用小驼峰式命名法（lower camel case），个别特殊定义文件夹除外，例如：components-xx

5. 每个文件要写注释