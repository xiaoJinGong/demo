const room = {
    name: 'room',
    txt: '格局',
    data: [
        { id: 1, txt: '一房' },
        { id: 2, txt: '二房' },
        { id: 3, txt: '三房' },
        { id: 4, txt: '四房' },
        { id: 5, txt: '五房' }
    ]
}

const area = {
    name: 'area',
    txt: '坪數',
    data: [
        { id: 11, txt: '10坪以下' },
        { id: 12, txt: '10-20坪' },
        { id: 2, txt: '20-30坪' },
        { id: 3, txt: '30-40坪' },
        { id: 4, txt: '40-50坪' },
        { id: 5, txt: '50坪上' }
    ]
}

const shape = {
    name: 'shape',
    txt: '型態',
    data: [
        { id: 1, txt: '公寓' },
        { id: 2, txt: '電梯大樓' },
        { id: 3, txt: '透天厝' },
        { id: 4, txt: '別墅' }
    ]
}

const floor = {
    name: 'newfloor',
    txt: '樓層',
    data: [
        { id: 1, txt: '1樓以下' },
        { id: 2, txt: '1-3樓' },
        { id: 3, txt: '4-6樓' },
        { id: 4, txt: '7-9樓' },
        { id: 5, txt: '10-12樓' },
        { id: 6, txt: '12樓以上' }
    ]
}

// const sex = {
//     name: 'sex',
//     txt: '性別',
//     data: [
//         { id: 0, txt: '不限' },
//         { id: 1, txt: '男' },
//         { id: 2, txt: '女' }
//     ]
// }

// const houseage = {
//     name: 'houseage',
//     txt: '屋齢',
//     data: [
//         { id: 1, txt: '5年以下' },
//         { id: 2, txt: '5-10年' },
//         { id: 3, txt: '10-20年' },
//         { id: 4, txt: '20-30年' },
//         { id: 5, txt: '30-40年' },
//         { id: 6, txt: '40年以上' }

//     ]
// }

const source = {
    name: 'source',
    txt: '經辦人',
    data: [
        { id: 1, txt: '屋主' },
        { id: 3, txt: '仲介' },
        { id: 2, txt: '代理人' }
    ]

}

export const OthersData = {
    name: 'others',
    txt: '其他',
    isMulti: true,
    data: [
        { id: 'parking', txt: '有車位' },
        { id: 'balcony_1', txt: '有陽台' },
        { id: 'pet', txt: '可養寵物' },
        { id: 'cook', txt: '可開伙' },
        { id: 'ex_top_floor', txt: '排除頂樓加蓋' }
    ]
}

export const KindData = {
    name: 'kind',
    txt: '型態',
    data: [
        { id: -1, txt: '不限' },
        { id: 1, txt: '整層住家' },
        { id: 2, txt: '獨立套房' },
        { id: 3, txt: '分租套房' },
        { id: 4, txt: '雅房' },
        { id: 5, txt: '店面' },
        { id: 6, txt: '辦公' },
        { id: 7, txt: '住辦' },
        { id: 8, txt: '廠房' },
        { id: 9, txt: '車位' },
        { id: 10, txt: '土地' }
    ]
}

export const MoreData = [room, area, shape, floor, source]

export const SortData = [
    { id: -1, txt: '默認排序' },
    { id: 11, txt: '價格從低到高' },
    { id: 12, txt: '價格從高到低' },
    { id: 22, txt: '坪數從大到小' },
    { id: 21, txt: '坪數從小到大' },
    { id: 32, txt: '刊登時間優先' }
]

export const changeDataName = [
    'region',
    'section',
    'metro',
    'station',
    'kind',
    'price',
    'room',
    'area',
    'shape',
    'floor',
    'source',
    'others',
    'sex',
    'age'
]
