/*
 *  捷運線和捷運站配置
 *  author      yulei@addcn.com
 *  updateTime  2016-04-21
 */

export let Metro = []
export let MetroPlane = []
export let Station = []

Metro[0] = [
    { id: 100, txt: '文山內湖線' },
    { id: 125, txt: '淡水線' },
    { id: 148, txt: '小南門線' },
    { id: 152, txt: '新店線' },
    { id: 162, txt: '中和線' },
    { id: 168, txt: '南港線' },
    { id: 181, txt: '板橋線' },
    { id: 187, txt: '土城線' },
    { id: 192, txt: '小碧潭支線' },
    { id: 195, txt: '新北投支線' },
    { id: 198, txt: '信義線' },
    { id: 206, txt: '松山線' },
    { id: 215, txt: '新莊線' },
    { id: 232, txt: '蘆洲線' }
]

Metro[1] = [
    { id: 238, txt: '捷運紅線' },
    { id: 263, txt: '捷運橘線' }
]

Metro[2] = [
    { id: 278, txt: '機場捷運' }
]

MetroPlane = [
    { id: 100, txt: '文山內湖線' },
    { id: 125, txt: '淡水線' },
    { id: 148, txt: '小南門線' },
    { id: 152, txt: '新店線' },
    { id: 162, txt: '中和線' },
    { id: 168, txt: '南港線' },
    { id: 181, txt: '板橋線' },
    { id: 187, txt: '土城線' },
    { id: 192, txt: '小碧潭支線' },
    { id: 195, txt: '新北投支線' },
    { id: 198, txt: '信義線' },
    { id: 206, txt: '松山線' },
    { id: 215, txt: '新莊線' },
    { id: 232, txt: '蘆洲線' },
    { id: 238, txt: '捷運紅線' },
    { id: 263, txt: '捷運橘線' },
    { id: 278, txt: '機場捷運' }
]

// 具體區域

Station[100] = [
    { id: 4257, txt: '南港展覽館' },
    { id: 4314, txt: '南港軟體園' },
    { id: 4315, txt: '東湖站' },
    { id: 4316, txt: '葫洲站' },
    { id: 4317, txt: '大湖公園' },
    { id: 4318, txt: '內湖站' },
    { id: 4319, txt: '文德站' },
    { id: 4320, txt: '港乾站' },
    { id: 4321, txt: '西湖站' },
    { id: 4282, txt: '劍南路站' },
    { id: 4323, txt: '大直站' },
    { id: 4324, txt: '松山機場' },
    { id: 4185, txt: '中山國中' },
    { id: 4186, txt: '南京東路' },
    { id: 4187, txt: '忠孝復興' },
    { id: 4188, txt: '大安站' },
    { id: 4189, txt: '科技大樓' },
    { id: 4190, txt: '六張犁站' },
    { id: 4191, txt: '麟光站' },
    { id: 4192, txt: '辛亥站' },
    { id: 4193, txt: '萬芳醫院' },
    { id: 4194, txt: '萬芳社區' },
    { id: 4195, txt: '木柵站' },
    { id: 4196, txt: '動物園站' }
]

Station[125] = [
    { id: 4163, txt: '淡水站' },
    { id: 4164, txt: '紅樹林站' },
    { id: 4165, txt: '竹圍站' },
    { id: 4166, txt: '關渡站' },
    { id: 4167, txt: '忠義站' },
    { id: 4168, txt: '復興崗站' },
    { id: 4169, txt: '北投站' },
    { id: 4170, txt: '奇岩站' },
    { id: 4171, txt: '奇哩岸站' },
    { id: 4172, txt: '石牌站' },
    { id: 4173, txt: '明德站' },
    { id: 4174, txt: '芝山站' },
    { id: 4175, txt: '士林站' },
    { id: 4176, txt: '劍潭站' },
    { id: 4177, txt: '圓山站' },
    { id: 4178, txt: '民權西路' },
    { id: 4179, txt: '雙連站' },
    { id: 4180, txt: '中山站' },
    { id: 4181, txt: '台北車站' },
    { id: 4182, txt: '台大醫院' },
    { id: 4183, txt: '中正紀念堂' },
    { id: 4184, txt: '古亭站' }
]
Station[148] = [
    { id: 4242, txt: '西門站' },
    { id: 4255, txt: '小南門站' },
    { id: 4183, txt: '中正紀念堂' }
]
Station[152] = [
    { id: 4184, txt: '古亭站' },
    { id: 4244, txt: '台電大樓' },
    { id: 4245, txt: '公館站' },
    { id: 4246, txt: '萬隆站' },
    { id: 4247, txt: '景美站' },
    { id: 4248, txt: '大坪林站' },
    { id: 4249, txt: '七張站' },
    { id: 4251, txt: '新店市公所' },
    { id: 4250, txt: '新店站' }
]
Station[162] = [
    { id: 4184, txt: '古亭站' },
    { id: 4231, txt: '頂溪站' },
    { id: 4232, txt: '永安市場' },
    { id: 4233, txt: '景安站' },
    { id: 4234, txt: '南勢角站' }
]
Station[168] = [
    { id: 4242, txt: '西門站' },
    { id: 4181, txt: '台北車站' },
    { id: 4267, txt: '善導寺站' },
    { id: 4221, txt: '忠孝新生' },
    { id: 4187, txt: '忠孝復興' },
    { id: 4264, txt: '忠孝敦化' },
    { id: 4263, txt: '國父紀念館' },
    { id: 4262, txt: '市政府站' },
    { id: 4261, txt: '永春站' },
    { id: 4260, txt: '後山埤站' },
    { id: 4259, txt: '昆陽站' },
    { id: 4258, txt: '南港站' }
]
Station[181] = [
    { id: 4275, txt: '府中站' },
    { id: 4274, txt: '板橋站' },
    { id: 4273, txt: '新埔站' },
    { id: 4272, txt: '江子翠站' },
    { id: 4271, txt: '龍山寺站' }
]
Station[187] = [
    { id: 4280, txt: '永寧站' },
    { id: 4279, txt: '土城站' },
    { id: 4278, txt: '海山站' },
    { id: 4277, txt: '亞東醫院' },
    { id: 4281, txt: '頂埔站' }
]
Station[192] = [
    { id: 4249, txt: '七張站' },
    { id: 4253, txt: '小碧潭站' }
]
Station[195] = [
    { id: 4169, txt: '北投站' },
    { id: 4198, txt: '新北投站' }
]
Station[198] = [
    { id: 4183, txt: '中正紀念堂' },
    { id: 4200, txt: '東門站' },
    { id: 4201, txt: '大安森林公園' },
    { id: 4188, txt: '大安站' },
    { id: 66300, txt: '信義安和站' },
    { id: 66301, txt: '世貿中心' },
    { id: 4205, txt: '象山站' }
]
Station[206] = [
    { id: 4242, txt: '西門站' },
    { id: 4241, txt: '北門站' },
    { id: 4180, txt: '中山站' },
    { id: 66266, txt: '松江南京' },
    { id: 4186, txt: '南京東路' },
    { id: 4237, txt: '台北小巨蛋' },
    { id: 4236, txt: '南京三民' },
    { id: 4235, txt: '松山站' }
]
Station[215] = [
    { id: 4200, txt: '東門站' },
    { id: 4221, txt: '忠孝新生' },
    { id: 66266, txt: '松江南京' },
    { id: 66265, txt: '行天宮站' },
    { id: 66264, txt: '中山國小' },
    { id: 4178, txt: '民權西路' },
    { id: 4216, txt: '大橋頭' },
    { id: 4215, txt: '台北橋站' },
    { id: 4214, txt: '菜寮站' },
    { id: 4213, txt: '三重站' },
    { id: 4212, txt: '先嗇宮站' },
    { id: 4211, txt: '頭前庄站' },
    { id: 4210, txt: '新莊站' },
    { id: 4209, txt: '輔大站' },
    { id: 4208, txt: '丹鳳站' },
    { id: 4207, txt: '迴龍站' }
]
Station[232] = [
    { id: 66262, txt: '三重國小' },
    { id: 66261, txt: '三和國中' },
    { id: 66260, txt: '徐匯中學' },
    { id: 66259, txt: '三民國中' },
    { id: 66258, txt: '蘆洲站' },
    { id: 292, txt: '大橋頭' },
    { id: 66264, txt: '中山國小' },
    { id: 66265, txt: '行天宮' },
    { id: 66266, txt: '松江南京站' }
]
Station[238] = [
    { id: 4329, txt: '小港站' },
    { id: 4330, txt: '高雄機場' },
    { id: 4331, txt: '草衙站' },
    { id: 4332, txt: '前鎮高中' },
    { id: 4333, txt: '凱旋站' },
    { id: 4334, txt: '獅甲站' },
    { id: 4335, txt: '三多商圈' },
    { id: 4336, txt: '中央公園' },
    { id: 4337, txt: '美麗島站' },
    { id: 4338, txt: '高雄車站' },
    { id: 4339, txt: '後驛站' },
    { id: 4340, txt: '凹子底站' },
    { id: 4341, txt: '巨蛋站' },
    { id: 4342, txt: '生態園區' },
    { id: 4343, txt: '左營站' },
    { id: 4344, txt: '世運站' },
    { id: 4345, txt: '油廠國小' },
    { id: 4346, txt: '楠梓加工區' },
    { id: 4347, txt: '後勁站' },
    { id: 4348, txt: '都會公園' },
    { id: 4349, txt: '青埔站' },
    { id: 4350, txt: '橋頭糖廠' },
    { id: 4351, txt: '橋頭火車' },
    { id: 4352, txt: '南岡山站' }
]
Station[263] = [
    { id: 4353, txt: '西子灣站' },
    { id: 4354, txt: '鹽埕埔站' },
    { id: 4355, txt: '市議會站' },
    { id: 4337, txt: '美麗島站' },
    { id: 4357, txt: '信義國小' },
    { id: 4358, txt: '文化中心' },
    { id: 4359, txt: '五塊厝站' },
    { id: 4360, txt: '技擊館站' },
    { id: 4361, txt: '衛武營站' },
    { id: 4362, txt: '鳳山西站' },
    { id: 4363, txt: '鳳山站' },
    { id: 4364, txt: '大東站' },
    { id: 4365, txt: '鳳山國中' },
    { id: 4366, txt: '大寮站' }
]

Station[278] = [
    { id: 66313, txt: '台北車站' },
    { id: 66316, txt: '三重站' },
    { id: 66317, txt: '新北產業園區' },
    { id: 66318, txt: '新莊副都心' },
    { id: 66319, txt: '泰山站' },
    { id: 66320, txt: '泰山貴和' },
    { id: 66321, txt: '體育大學' },
    { id: 66335, txt: '長庚醫院' },
    { id: 66322, txt: '林口站' },
    { id: 66323, txt: '山鼻站' },
    { id: 66324, txt: '坑口站' },
    { id: 66325, txt: '第一航廈' },
    { id: 66326, txt: '第二航廈' },
    { id: 66327, txt: '機場旅館站' },
    { id: 66328, txt: '大園站' },
    { id: 66329, txt: '橫山站' },
    { id: 66330, txt: '領航站' },
    { id: 66331, txt: '高鐵桃園站' },
    { id: 66332, txt: '桃園體育園區' },
    { id: 66333, txt: '興南站' },
    { id: 66334, txt: '環北站' }
]
