/*
 * @Description: 筛选处理Txt，返回筛选后的数据Object
 * @Author: Jino
 * @Date: 2020-06-13 14:23:50
 * @LastEditors: Jino
 * @LastEditTime: 2020-06-15 10:15:42
 */ 

import { Region } from './../assets/js/region';
import { Section } from './../assets/js/section';
import { MetroPlane, Station } from './../assets/js/metro';
import { KindData, MoreData, OthersData } from './../assets/js/other';

import { getRequest, getQueryValue, getScreenPrice } from './index';

/**
 *  需要返回的数据初始化
*/
let titleArray = [
  {
    id: 1, // 1: 地址展示, 2: 类型展示, 3: 租金展示, 4: 更多展示
    isShow: false,
    txt: '區域',
    titleTxt: '' // 页面顶部title显示 document.title
  },
  {
    id: 2,
    isShow: false,
    txt: '類型',
    titleTxt: '' // title显示
  },
  {
    id: 3,
    isShow: false,
    txt: '租金',
    titleTxt: '' // title显示
  },
  {
    id: 4,
    isShow: false,
    txt: '更多',
    titleTxt: '' // title显示
  }
];

// 初始化复制保存起来，以解决每次删除的时候title数据不还原BUG
let localTitle = JSON.stringify(titleArray);

/**
* 处理筛选titleArray的文字
* params: {
*  data: Array, 当前所展示的data
*  type: Number, 1: 地址展示, 2: 类型展示, 3: 租金展示, 4: 更多展示
*  val: any, 当前所展示对应的id
* }
*/
function changeScreenTxt(data,type,val){
  let obj,titleTxt;
  let nowObj = JSON.stringify(titleArray[type - 1]);
  
  if ( type === 4 ) { // 当前显示的更多

    // 处理要显示的页面title
    titleTxt = JSON.parse(nowObj).titleTxt;
    if ( data && data.length > 0 ) {
      obj = data.find((item)=> item.id == val);
      titleTxt = titleTxt + obj.txt + '、';
    }

    // type减一是为了查找对应的索引，索引都会 -1
    titleArray[type-1] = Object.assign(titleArray[type-1], {
      isShow: true,
      txt: `更多(${val})`,
      titleTxt: titleTxt
    })

    return;

  } else if ( type === 3 ) { // 当前是租金显示

    obj = getScreenPrice(val);
    titleTxt = !obj.priceCustom && JSON.parse(nowObj).titleTxt + obj.txt;

  } else { // 剩余显示

    obj = data.find((item)=> item.id == val);
    titleTxt = JSON.parse(nowObj).titleTxt + obj.txt;

  }

  titleArray[type-1] = Object.assign(titleArray[type-1], {
    isShow: true,
    txt: (obj.txt && obj.txt.length <= 4) ? obj.txt : obj.txt.substring(0,4) + "...",
    titleTxt: titleTxt
  })

}

/**
 * 篩選選中的文字
 * @params  无
 * @return  Object 进行处理后的筛选对象
*/
function getScreenObj () {
  
  titleArray = JSON.parse(localTitle); // 初始化复制保存起来，以解决每次删除的时候title数据不还原BUG

  let allQuery = getRequest();
  let num = 0; // 更多展示的个数
  let currId;
  let moreNum = true;
  for(let key in allQuery){

    let keyVal = allQuery[key];
    
    if (key === 'regionid') { // 区域中选中，也就是县市级

      // 获取区域二级id，也就是县级
      currId = getQueryValue('sectionidStr');
      changeScreenTxt(Region, 1, keyVal);
      (currId) && changeScreenTxt(Section[keyVal], 1, Number(currId));

    } else if (key === 'mrtcoods') { // 区域中捷运选中，也就是地铁
      
        currId = getQueryValue('mrtstation');
        changeScreenTxt(MetroPlane, 1, currId);
        changeScreenTxt(Station[currId], 1, keyVal);

    } else if (key === 'kind') { // 类型选中

      changeScreenTxt(KindData.data, 2, keyVal);

    } else if (key === 'price') { // 租金选中

      changeScreenTxt(null, 3, keyVal);

    } else if ( MoreData.find( (item)=> item.name === key ) ) { // 更多处理

      num ++;
      // 加上页面title
      changeScreenTxt( MoreData.find((item)=> item.name === key).data, 4, keyVal );

    } else if( OthersData.data.find((item)=> item.id === key) ) { // 更多里面的其它选项处理

      if(moreNum) {
        moreNum = false;
        num ++;
      }
      // 加上页面title
      changeScreenTxt(OthersData.data, 4, key);

    }
  }
  // 在有数据的时候才会改变
  (num > 0) && changeScreenTxt(null, 4, num);

  return titleArray;

}

export default getScreenObj;