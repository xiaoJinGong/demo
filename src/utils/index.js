
/**
 * [获取URL中的参数名及参数值的集合]
 * @param {[string]} urlStr [当该参数不为空的时候，则解析该url中的参数集合]
 * @return {[string]}       [参数集合]
*/

import Router from './../router';
import { RentPrice } from './../assets/js/price';

function getRequest(urlStr) {
  let url;
  if (typeof urlStr == "undefined") {
    url = decodeURI(location.search); //获取url中"?"符后的字符串
  } else {
    url = "?" + urlStr.split("?")[1];
  }
  let theRequest = {};
  if (url.indexOf("?") != -1) {
    let str = url.substr(1);
    let strs = str.split("&");
    for (let i = 0; i < strs.length; i++) {
      theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
    }
  }
  return theRequest;
}

/**
 * [通过参数名获取url中的参数值]
 * @param  {[string]} queryName [参数名]
 * @return {[string]}           [参数值]
 */
function getQueryValue(queryName) {
  var query = decodeURIComponent(window.location.search.substring(1));
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (pair[0] == queryName) { return pair[1]; }
  }
  return null;
}

/**
 * [改变url]
 * @param  {[object]}  [参数名]
 * @return {[object]}    [参数值]
*/
function changeUrl(obj){
  let allQuery = getRequest();
  let assObj = Object.assign(allQuery, obj);

  for(let key in assObj){
    let keyVal = assObj[key];
    if( key === 'regionid'){ // 区域选择
      if(keyVal == 0){ // 市级不限选中
        delete assObj['regionid'];
        delete assObj['sectionidStr'];
      } else {
        delete assObj['region'];
        delete assObj['section'];
      }
      if(obj.mrtcoods){ // 捷运选中
        delete assObj['regionid'];
        delete assObj['sectionidStr'];
        delete assObj['region'];
        delete assObj['section'];
      } else {  // 市级选中
        delete assObj['mrtstation'];
        delete assObj['mrtcoods'];
        delete assObj['searchtype'];
        delete assObj['mrt'];
      }
    } else {
      if(keyVal == -1){ // 其余的选择，当值为-1时，即会删掉当前key值
        delete assObj[key];
      }
    }
  }

  if(assObj && assObj.constructor === Object && assObj instanceof Object){
    let urlStr = '';
    for(let key  in assObj){
      urlStr += `${key}=${assObj[key]}&`;
    }
    let newUrl = '?' + urlStr.slice(0,-1);
    Router.replace({path:`${newUrl}`})
  }
}

/**
 * [设置是否滚动]
 * @param  {[Number]} type [参数名] 默认不传
 * @return {[string]}     [参数值]
*/
function setStopScroll(type){
  let body = document.documentElement.body || document.body;
  if( type === 1){ // 可以滚动
    body.setAttribute('style', '');
  } else { // 静止滚动
    body.setAttribute('style', 'overflow: hidden;');
  }
}

/**
 * [设置展示taost]
 * @param  {[String]} text [参数名] 必传
 * @return {[string]}      [参数值]
*/
function showTaost(text){
  let app = document.getElementById("app");
  let div = document.createElement('div');
  div.innerHTML = text;
  div.className = 'toast';
  app.appendChild(div);
  let timer = setTimeout(()=>{
    app.removeChild(div);
    clearTimeout(timer);
  },3000)
}

/**
 * [设置滚动到当前索引]
 * @param  {[Object,Object]} [父元素,当前索引元素] 必传
 * @return {[string]}     [参数值]
*/
function setScrollIndex(parentEle,el){
  // 判断是否存在该元素
  if(!parentEle || !el){
    return;
  }
  // 获取父元素的整体高度
  let parentHeight = parentEle.clientHeight;
  // 计算出除了当前元素剩余的高度
  let elHeight = el.clientHeight;
  let diff = parentHeight - elHeight;
  // 当前元素距离父元素顶部的高度
  let elTop = el.offsetTop;
  // 计算出需要滚动的高度
  let scrollHeight = elTop - diff;
  // 设置滚动到当前索引
  parentEle.scrollTop = scrollHeight;
}

/**
 * [设置滚动到页面顶部]
 * @param  {[]} 
 * @return {[string]}     [参数值]
*/
function setScrollTop(){
  document.documentElement.scrollTop = 0
  document.body.scrollTop = 0;
}

/**
 * 筛选价格title
 * @params {
 *  keyVal: String 当前价格对应的val
 * }
 * @return  Object 进行处理后的价格数据
 * 
*/
function getScreenPrice (keyVal) {
  
  let obj = {
    id: '', // 价格对应id
    txt: '', // 价格显示文本
    minPrice: '', // 最小价格
    maxPrice: '', // 最大价格
    priceCustom: false // 价格是不是自定义
  };
  
  if(!keyVal) return obj;

  if(keyVal){
    obj.txt = keyVal;
    obj.priceCustom = true;
    RentPrice.forEach((item)=>{
      if(item.id === keyVal){
        obj.id = item.id;
        obj.txt = item.txt;
        obj.priceCustom = false;
      }
    });
    if(obj.priceCustom){ // 如果是自定义
      let nowArr = keyVal.split('$');
      let submin = nowArr[0];
      let subMax = nowArr[1].replace('_','');
      if(subMax){
        obj.maxPrice = subMax;
        obj.txt = subMax + "元以下";
      } 
      if(submin){
        obj.minPrice = submin;
        obj.txt = submin + "元以上";
      }
      if(subMax && submin){
        obj.txt = submin + '-' + subMax + "元";
      }
    }
  }
  return obj;
}


export {
  getRequest,
  getQueryValue,
  changeUrl,
  setStopScroll,
  showTaost,
  setScrollIndex,
  setScrollTop,
  getScreenPrice
}