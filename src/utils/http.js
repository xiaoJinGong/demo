/*
 * @Description: http 请求封装
 * @Author: Jino
 * @Date: 2020-06-02 15:31:56
 * @LastEditors: Jino
 * @LastEditTime: 2020-06-10 19:22:49
 */ 

import axios from 'axios';

//请求实列
let http = null;

//创建实列方法
const install = (options = {}) => {
    http = axios.create(options);
};

export {
  http,
  install
};

install({
  baseURL: '/api',
  message: '加载中...', //{string}变更Toast中提示文字，默认 加载中
  isMessage: true, // {boolean}是否显示加载中，默认显示
  addToken: true, //{boolean} 接口访问是否需要添加token，默认添加true
  timeout: 30000, //{number}   请求超时时间 默认 30000
  params: null, //{object}  get方式传参key值
  headers: {
    'Content-Type': "application/json"
  }, //{string} 指定请求头信息
  withCredentials: true //{boolean} 请求是否携带本地cookies信息默认开启
});

// 添加请求拦截器
http.interceptors.request.use(function(config) {
  // console.log(config, 'config');
  return config;
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
http.interceptors.response.use(function(response) {
  // console.log(response, 'response');
  return response.data;
},function(error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});
