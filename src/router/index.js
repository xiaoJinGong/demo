/*
 * @Description: 路由配置
 * @Author: Jino
 * @Date: 2020-06-08 16:20:59
 * @LastEditors: Jino
 * @LastEditTime: 2020-06-11 16:10:06
 */ 

import Vue from 'vue'
import Router from 'vue-router'
import Index from './../pages/Index.vue'

Vue.use(Router)

// 解决路有重复加载时报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

// 解决路有重复加载时报错问题
const originalReplace = Router.prototype.replace;
Router.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err);
};

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        title: ' |关于'   // 标题设置
      }
    }
  ],
})

export default router

